from . import db
import datetime
import binascii

class Account(db.Model):
    __tablename__ = "users"

    # pylint: disable=no-member
    name = db.Column(db.String(255), unique=True, primary_key=True)

    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))

    public_key = db.Column(db.String(255))
    created = db.Column(db.DateTime())

    document_type = db.Column(db.String(255))   # example: student_card
    document_number = db.Column(db.String(255)) # example: stundent card serial number
    document_key = db.Column(db.Binary())       # some students cards can store some binary
                                                # data which can be readed by NFC reader
    email = db.Column(db.String(255))
    approved = db.Column(db.Boolean())

    owner_account_id = db.Column(db.String(255))
    disabled = db.Column(db.Boolean())

    def __init__(self, init_dict):
        self.name = init_dict['name']
        self.created = datetime.datetime.now()

        self.email = init_dict['email']

        self.first_name = init_dict['first_name']
        self.last_name = init_dict['last_name']

        self.public_key = init_dict['public_key']
        self.owner_account_id = init_dict['owner_account_id']
        self.document_type = init_dict['document']['type'].lower()
        self.document_number = init_dict['document']['number']
        self.document_key = init_dict['document'].get('key')
        self.approved = False
        self.disabled = False


    def get_account_info(self):
        return {
            'public_key': self.public_key,
            'email': self.email,
            'name': self.name,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'approved': self.approved,
            'created': self.created,
            'document_number': self.document_number,
            'owner_account_id': self.owner_account_id,
            'disabled': self.disabled
        }

    def commit(self):
        db.session.add(self)
        db.session.commit()