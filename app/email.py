import smtplib
import email.utils
import logging
from email.mime.text import MIMEText

def send(text, subject, recipient, author):
    logging.debug("sending mail: %s %s %s %s", text, subject, recipient, author)
    # Create the message
    msg = MIMEText(text)
    msg['To'] = email.utils.formataddr(('Recipient', 'recipient@example.com'))
    msg['From'] = email.utils.formataddr(('Author', author))
    msg['Subject'] = subject

    server = smtplib.SMTP('127.0.0.1', 1025)
    server.set_debuglevel(True)  # show communication with the server
    try:
        server.sendmail(author
                        [recipient],
                        msg.as_string())
    finally:
        server.quit()