from flask import render_template, request, jsonify, abort
from . import app, models
import traceback
from . import config
from . import bitshares
import binascii
from graphenebase.account import PasswordKey
import sqlalchemy
from . import db
import time
from . import email

log = app.logger


def api_error(msg, code=400):
    log.error(msg)
    return jsonify({"error": msg}), code


@app.route('/')
def index():
    return render_template('index.html')

def verify_signature():
    try:
        query_args = {key: value for key, value in request.args.items()}
        bts = bitshares.BitShares(nobroadcast=config.nobroadcast)
        signature = binascii.a2b_hex(query_args.get('req_signature'))
        account = query_args.get('req_account')
        unix_minutes = int(time.time()) // 60
        data = bytes([unix_minutes >> i & 0xff for i in (24,16,8,0)])

        log.debug('checking %d %d:%s %s', unix_minutes, len(signature), signature, account)
        return signature and account and bts.check_signature(signature, data, account)
    except Exception as e:
        log.error(str(e))
        return False
required_fields = ["first_name", "last_name", "document", "email", "public_key"]

@app.route('/api/v1/accounts', methods=['POST'])
def post_account():
    log.debug('received signup request: %s', request.json)
    # make sure all keys are present
    missing_fields = list(filter(lambda x: True if type(x) is str else False,
        [key if key not in request.get_json() else None for key in required_fields]))

    if len(missing_fields) > 0:
        return api_error('not all required fields where presented in account info: %s' % missing_fields)

    account = request.get_json()
    account_name = account['email']
    log.debug('searching for account name %s conflicts', account_name)

    bts = bitshares.BitShares(nobroadcast=config.nobroadcast)
    if bts.check_account_exists(account_name) or models.Account.query.get(account_name):
        return api_error("account with email {email} is already exists".format(**account))

    account['name'] = account_name

    owner = bts.rpc.get_account_by_name(config['owner_account'])
    account['owner_account_id'] = owner['id']

    try:
        account_model = models.Account(account)
        account_model.commit()

    except sqlalchemy.exc.IntegrityError as e:
        return api_error("can't place registration request into database: %s" % e)

    return jsonify({"name": account['name']}), 201


@app.route('/api/v1/accounts/approve', methods=['POST'])
def approve():
    if not verify_signature():
        return api_error('Not authorized', 401)

    filter = request.get_json()
    log.debug(filter)
    models.Account.query.filter_by(**filter).update({"approved": 1})

    for account_model in models.Account.query.filter_by(**filter).all():
        try:
            email.send(
                text=config["mail_notifications"]["approval"]["text"].format(**account_model.get_account_info()),
                subject=config["mail_notifications"]["approval"]["subject"],
                recipient=account_model.email,
                author=config["mail_notifications"]["author"]
            )
        except Exception as e:
            log.error(e)

    return jsonify({"name": filter["name"]}), 200

@app.route('/api/v1/accounts/approve', methods=['GET'])
def get_approve():
    if not request.args.get('name'):
        return api_error("invalid query", 400)

    m = models.Account.query.filter_by(**{'name': request.args.get('name')}).first()
    if not m:
        return api_error("account %s not found" % request.args.get('name'), 404)
    else:
        return jsonify({
            "approved": m.approved,
            "name": m.name
        }), 200

@app.route('/api/v1/accounts/account_name_by_email', methods=['GET'])
def get_account_name_by_email():
    if not request.args.get('email'):
        return api_error("invalid query", 400)

    m = models.Account.query.filter_by(**{'email': request.args.get('email')}).first()
    if not m:
        return api_error("account with email=%s not found" % request.args.get('email'), 404)
    else:
        return jsonify({
            "name": m.name
        }), 200

@app.route('/api/v1/accounts', methods=['GET', 'OPTIONS'])
def get_accounts():
    if not verify_signature():
        return api_error('Not authorized', 401)

    args = {key: value for key, value in request.args.items() if key in models.Account.__table__.columns}
    users = models.Account.query.filter_by(**args).all()
    return jsonify({'accounts': [u.get_account_info() for u in users]}), 200

@app.route('/api/v1/accounts/<account_name>', methods=['DELETE'])
def remove_account_request(account_name):
    if not verify_signature():
        return api_error('Not authorized', 401)

    account = models.Account.query.filter_by(name=account_name).update({"disabled" : True})
    print("remove_account_request: disabled request for accout - %s" % account_name)
    return jsonify({"name": account_name}), 200