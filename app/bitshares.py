from grapheneapi.graphenewsrpc import GrapheneWebsocketRPC
from graphenebase import transactions
from graphenebase.account import PublicKey
from . import config, app
import ecdsa
import binascii
import hashlib

log = app.logger

GRAPHENE_1_PERCENT = (10000 / 100)


def ecdsa_pub_key_to_graphene_pub_key(ecdsa_pub_key):
    order = ecdsa_pub_key.curve.generator.order()
    p = ecdsa_pub_key.pubkey.point
    x_str = ecdsa.util.number_to_string(p.x(), order)
    return bytes(chr(2 + (p.y() & 1)), 'ascii') + x_str

class APIUnavailable(Exception):
    pass


class BroadcastingError(Exception):
    pass


class MissingKeyError(Exception):
    pass


class MissingRegistrarError(Exception):
    pass


class MissingPublicKeys(Exception):
    pass


class WrongNetworkError(Exception):
    pass


class BitShares():

    prefix = None
    rpc = None

    def __init__(self, *args, **kwargs):
        self.nobroadcast = kwargs.pop("nobroadcast", False)
        try:
            self.rpc = GrapheneWebsocketRPC(config.witness_url, **kwargs)
            self.prefix = config.get("prefix")
        except:
            raise APIUnavailable("API node seems to be down!")

        self.chain_id = self.rpc.get_chain_id()

    def executeOp(self, op, wif):
        ops = [transactions.Operation(op)]
        # pylint: disable=no-member
        ops = transactions.addRequiredFees(self.rpc, ops, "1.3.0")
        expiration = transactions.formatTimeFromNow(30)
        ref_block_num, ref_block_prefix = transactions.getBlockParams(self.rpc)
        tx = transactions.Signed_Transaction(
            ref_block_num=ref_block_num,
            ref_block_prefix=ref_block_prefix,
            expiration=expiration,
            operations=ops
        )
        if "chain" in config:
            tx = tx.sign([wif], self.chain_id)
        else:
            tx = tx.sign([wif], self.chain_id)
        # pylint: disable=no-member
        tx = transactions.JsonObj(tx)

        if not self.nobroadcast:
            try:
                self.rpc.broadcast_transaction(tx, api="network_broadcast")
            except:
                import traceback
                print(traceback.format_exc())
                raise BroadcastingError("Broadcasting error")
        else:
            print("Not broadcasting anything!")

        return tx

    def check_account_exists(self, name):
        try:
            account = self.rpc.get_account_by_name(name, num_retries=0)
            if account:
                return True
            else:
                return False
        except:
            account = self.rpc.get_account_by_name(name)
            return False

    def get_balance(self):
        asset = self.rpc.get_asset("1.3.0")
        account = self.rpc.get_account(config.registrar)
        for b in self.rpc.get_account_balances(
            account["id"],
            []
        ):
            if b["asset_id"] == "1.3.0":
                return int(b["amount"]) / 10 ** asset["precision"]

        return 0

    def create_account(self, data):
        name = data.get("name", None)

        owner_key = data.get("owner_key", None)
        active_key = data.get("active_key", None)
        memo_key = data.get("memo_key", None)

        if not owner_key or not active_key or not memo_key:
            raise MissingPublicKeys("Not all required public keys have been provided")

        if (
            owner_key[:len(self.prefix)] != self.prefix or
            active_key[:len(self.prefix)] != self.prefix or
            memo_key[:len(self.prefix)] != self.prefix
        ):
            raise WrongNetworkError(
                "This faucet is configured for prefix %s, but you are trying to register with key %s" % (self.prefix, owner_key)
            )

        # Get registrar
        registrar = data.get("registrar", config.registrar)

        # Get referrer
        referrer = data.get("referrer", None)
        if not referrer:
            referrer = config.default_referrer

        # Get referrer percentage
        referrer_percent = data.get("referrer_percent", config.referrer_percent)

        # Get wif
        wif = getattr(config, "wif", None)
        if not wif:
            raise MissingKeyError("We don't have a private key!")

        # Account data
        registrar = self.rpc.get_account(registrar)
        referrer = self.rpc.get_account(referrer)

        if not referrer:
            raise Exception("Unknown referrer!")
        if not registrar:
            raise Exception("Unknown registrar!")

        s = {"fee": {"amount": 100,
                     "asset_id": "1.3.0"
                     },
             "registrar": registrar["id"],
             "referrer": referrer["id"],
             "referrer_percent": int(referrer_percent * GRAPHENE_1_PERCENT),
             "name": name,
             "owner": {"weight_threshold": 1,
                       "account_auths": [],
                       'key_auths': [[owner_key, 1]],
                       "address_auths": []
                       },
             "active": {"weight_threshold": 1,
                        "account_auths": [],
                        'key_auths': [[active_key, 1]],
                        "address_auths": []
                        },
             "options": {
                         "memo_key": memo_key,
                         "voting_account": "1.2.5",
                         "num_witness": 0,
                         "num_committee": 0,
                         "votes": [],
                         "extensions": []
                         },
             "extensions": {},
             "prefix": self.prefix
             }
        try:
            # pylint: disable=no-member
            op = transactions.Account_create(**s)
        except Exception as e:
            log.error(
                "Error crearing account:\n\n%s\n\n%s" %
                (str(e), str(s))
            )
            raise e
        self.executeOp(op, wif)

    def check_signature(self, signature, data, account_name):
        account = self.rpc.get_account_by_name(account_name)

        if not account:
            log.error("account %s was not found", account_name)
            return False

        # base58_encoded_pub_key = account['active']['key_auths'][0][0]
        # account_gph_pub_key = PublicKey(base58_encoded_pub_key, config.get('prefix'))

        ecdsa_verivying_key = self.verify_gph_signature(signature, data)
        # gph_verifyin_key = ecdsa_pub_key_to_graphene_pub_key(ecdsa_verivying_key)

        return True

    def recover_public_key(self, digest, signature, i):
        """ Recover the public key from the the signature
        """
        # See http: //www.secg.org/download/aid-780/sec1-v2.pdf section 4.1.6 primarily
        curve = ecdsa.SECP256k1.curve
        G = ecdsa.SECP256k1.generator
        order = ecdsa.SECP256k1.order
        yp = (i % 2)
        r, s = ecdsa.util.sigdecode_string(signature, order)
        # 1.1
        x = r + (i // 2) * order
        # 1.3. This actually calculates for either effectively 02||X or 03||X depending on 'k' instead of always for 02||X as specified.
        # This substitutes for the lack of reversing R later     on. -R actually is defined to be just flipping the y-coordinate in the elliptic curve.
        alpha = ((x * x * x) + (curve.a() * x) + curve.b()) % curve.p()
        beta = ecdsa.numbertheory.square_root_mod_prime(alpha, curve.p())
        y = beta if (beta - yp) % 2 == 0 else curve.p() - beta
        # 1.4 Constructor of Point is supposed to check if nR is at infinity.
        R = ecdsa.ellipticcurve.Point(curve, x, y, order)
        # 1.5 Compute e
        e = ecdsa.util.string_to_number(digest)
        # 1.6 Compute Q = r^-1(sR - eG)
        Q = ecdsa.numbertheory.inverse_mod(r, order) * (s * R + (-e % order) * G)
        # Not strictly necessary, but let's verify the message for paranoia's sake.
        if not ecdsa.VerifyingKey.from_public_point(Q, curve=ecdsa.SECP256k1).verify_digest(signature, digest, sigdecode=ecdsa.util.sigdecode_string):
            return None
        return ecdsa.VerifyingKey.from_public_point(Q, curve=ecdsa.SECP256k1)


    def verify_gph_signature(self, signature, data):
        message = binascii.a2b_hex(self.chain_id) + bytes(data)
        digest = hashlib.sha256(message).digest()

        sig = bytes(signature)[1:]
        # recover parameter only
        recoverParameter = (bytes(signature)[0]) - 4 - 27
        p = self.recover_public_key(digest, sig, recoverParameter)
        # Will throw an exception of not valid
        p.verify_digest(sig, digest, sigdecode=ecdsa.util.sigdecode_string)
        return p
