# git repos
-e git+https://gitlab.com/qcoin/python-graphenelib@develop#egg=graphenelib
-e git://github.com/xeroc/python-bitshares@develop#egg=bitshares
autobahn
pycrypto
pyyaml

# flask
flask
Flask-SQLAlchemy
Flask-Session
flask-cors
flask-script
flask-mail

# hosting
gunicorn
configparser

# misc
requests>=2.9.1

pylint
